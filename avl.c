#include "avl.h"


Avl * cria_arv_vazia(void) {
		return NULL;
}

int verifica_arv_vazia(Avl * arv) {
		return (arv == NULL);
}

Avl * libera_arvore (Avl * arv) {
		if(!verifica_arv_vazia(arv)) {
				libera_arvore (arv->esq);
				libera_arvore (arv->dir);
				free(arv);
		}
		return NULL;
}

void imprime_in(Avl * arv) {
    if (!verifica_arv_vazia(arv))
    {
	    imprime_in (arv->esq);
		printf("%d ", arv->elemento.matricula);
        imprime_in (arv->dir);
    }
}

void imprime_pre(Avl * arv) {
    if (!verifica_arv_vazia(arv))
    {
		printf("%d ", arv->elemento.matricula);
        imprime_in (arv->esq);
        imprime_in (arv->dir);
    }
}

void imprime_pos(Avl * arv) {
    if (!verifica_arv_vazia(arv))
    {
		imprime_in (arv->esq);
        imprime_in (arv->dir);
		printf("%d ", arv->elemento.matricula);
    }
}

Avl * busca_elemento (Avl * arv, int matricula) {
		if(!verifica_arv_vazia(arv)) {
			if ( arv->elemento.matricula  == matricula ) {
					return arv;
			} else {
					if ( arv->elemento.matricula > matricula)
						return busca_elemento (arv->dir, matricula);
					if ( arv->elemento.matricula < matricula)
						return busca_elemento (arv->esq, matricula);
			}
		}
		return NULL;
}


Avl* inserir (Avl *a, Aluno aluno){

}

int max(int a, int b){
  return (a > b) ? a : b;
}

int calcula_altura_arvore(Avl *arvore){
  if (arvore_vazia(arvore)){
    return -1;
  }
  else{
    return max(calcula_altura_arvore(arvore->esq), calcula_altura_arvore(arvore->dir)) + 1;
  }
}

int calcula_balanceamento(Avl *arvore){
  return calcula_altura_arvore(arvore->dir) - calcula_altura_arvore(arvore->esq);
}
