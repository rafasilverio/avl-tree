#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

typedef struct aluno {
	int matricula;
	char nome[20];
	int notas[3];
} Aluno;

typedef struct avl {
	struct avl * dir;
	struct avl * esq;
	struct avl * pai;
	int fator_balanceamento;
	Aluno elemento;
} Avl;

Avl * cria_arv_vazia(void);
int verifica_arv_vazia(Avl * arv);
Avl * libera_arvore (Avl * arv);
void imprime_in(Avl * arv);
void imprime_pre(Avl * arv);
void imprime_pos(Avl * arv);
Avl * busca_elemento (Avl * arv, int matricula);
Avl * inserir(Avl * arv, Aluno aluno);
/*
Avl * remover(Avl * arv, Aluno aluno);
*/
